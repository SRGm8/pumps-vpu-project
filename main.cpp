/*
Disclaimer: This source code is based on several implementations provided by
Intel Corporation inside the Caffe and NCSDK frameworks. See "license" for more
information.
*/

#include "common.hpp"
#include "intelvpu.hpp"
#include "imagefolder_fp16.hpp"
#include "stopwatch.hpp"

using namespace MPI;
using std::string;

// GoogleNet image dimensions, network mean values for each channel in BGR order
const int networkDim = 224;
float networkMean[] = {0.40787054*255.0, 0.45752458*255.0, 0.48109378*255.0};

Stopwatch stopwatch_main(2);

int main(int argc, char** argv)
{
    std::vector<void *> images;
    
    // Make sure that the argument list is correct
    if (argc < 5)
    {
        return -1;
    }

    string graph_file = argv[1];
    string label_file = argv[2];
    string folder = argv[3];
    int num_devices = 2;
    sscanf(argv[4], "%d", &num_devices);
    images.resize(num_devices);
    
    SourceImage  *source = new ImageFolderFP16(folder.c_str(), networkDim,
                                               networkMean);
    TargetDevice *device = new IntelVPU(graph_file, label_file, num_devices,
                                        networkDim, networkMean);
                                              
    device->initNetwork();
    
    stopwatch_main.start(0);
    while (1)
    {
        stopwatch_main.start(1);
        for (int device_id = 0; device_id < num_devices; device_id++)
        {
            images[device_id] = (void *)source->getNextImage();
            
            if (images[device_id] == NULL)
            {
                break;
            }
        }
        stopwatch_main.stop(1);
        
        if (images[num_devices-1] == NULL)
        {
            break;
        }
        
        std::vector<Prediction> p = device->classify(images);
        
        for (int device_id = 0; device_id < num_devices; device_id++)
        {
            printf("%s; %.6f\n", p[device_id].first.c_str(), p[device_id].second);
            
            free(images[device_id]);
            images[device_id] = NULL;
        }
    }
    stopwatch_main.stop(0);
    
    printf("%.6f; %.6f; %zu\n", stopwatch_main.getElapsed(0, STOPWATCH_UNIT_MILLISECONDS),
                                stopwatch_main.getMean(1, STOPWATCH_UNIT_MILLISECONDS),
                                stopwatch_main.getElapsedCount(1));
    device->releaseNetwork();
    
    delete device;
    delete source;
    
    return 0;
}

