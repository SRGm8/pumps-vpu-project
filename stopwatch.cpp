
#include "common.hpp"
#include "stopwatch.hpp"

using namespace std;
using namespace MPI;

Stopwatch::Stopwatch(size_t size) :
    m_elapsed(size)
{
    
}

Stopwatch::~Stopwatch()
{
    
}

int Stopwatch::start(int id)
{
    StopwatchMeasurement measurement;
    CHK(gettimeofday(&measurement.start, NULL));
    
    m_elapsed[id].push_back(measurement);
    
    return S_OK;
}

int Stopwatch::stop(int id)
{
    return gettimeofday(&m_elapsed[id].back().stop, NULL);
}

int Stopwatch::restart(int id)
{
    m_elapsed[id].clear();
    
    return start(id);
}

double getElapsedMeasurement(StopwatchMeasurement &measurement, StopwatchUnit &unit)
{
    const struct timeval t[]     = { measurement.start, measurement.stop };
    const double         elapsed = (double)((t[1].tv_sec - t[0].tv_sec) * 1000000LL +
                                            (t[1].tv_usec - t[0].tv_usec));
    
    return elapsed / (double)unit;
}

double Stopwatch::getElapsed(int id, StopwatchUnit unit)
{
    double elapsed = 0.0;
    
    for (StopwatchMeasurementStoreIt it = m_elapsed[id].begin(); it != m_elapsed[id].end(); ++it)
    {
        elapsed += getElapsedMeasurement(*it, unit);
    }
    
    return elapsed;
}

size_t Stopwatch::getElapsedCount(int id)
{
    return m_elapsed[id].size();
}

double Stopwatch::getMean(int id, StopwatchUnit unit)
{
    return getElapsed(id, unit) / (double)getElapsedCount(id);
}

double Stopwatch::getMeanStd(int id, StopwatchUnit unit)
{
    const double mean = getMean(id, unit);
    double       acum = 0.0;
    
    if (mean > 0.0)
    {
        for (StopwatchMeasurementStoreIt it = m_elapsed[id].begin(); it != m_elapsed[id].end(); ++it)
        {
            const double elapsed = getElapsedMeasurement(*it, unit);
            const double diff    = (elapsed - mean);
            
            acum += (diff * diff);
        }
        
        acum /= (double)(m_elapsed[id].size() - 1);
    }
    
    return sqrtf(acum);
}

double Stopwatch::getMin(int id, StopwatchUnit unit)
{
    double min = DBL_MAX;
    
    for (StopwatchMeasurementStoreIt it = m_elapsed[id].begin(); it != m_elapsed[id].end(); ++it)
    {
        double elapsed = getElapsedMeasurement(*it, unit);
        
        if (elapsed < min)
        {
            min = elapsed;
        }
    }
    
    return min;
}

double Stopwatch::getMax(int id, StopwatchUnit unit)
{
    double max = 0.0;
    
    for (StopwatchMeasurementStoreIt it = m_elapsed[id].begin(); it != m_elapsed[id].end(); ++it)
    {
        double elapsed = getElapsedMeasurement(*it, unit);
        
        if (elapsed > max)
        {
            max = elapsed;
        }
    }
    
    return max;
}

