
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"
#include "fp16.h"
#include "imagefolder_fp16.hpp"

ImageFolderFP16::ImageFolderFP16(const char *_folder, int _reqsize, float *_mean)
{
    dir     = opendir(_folder);
    reqsize = _reqsize;
    strcpy(folder, _folder);
    memcpy(mean, _mean, sizeof(float)*3);
}

void *ImageFolderFP16::getNextImage()
{
    char filename[PATH_MAX];
    struct dirent *ent = NULL;
    
    do
    {
        ent = readdir(dir);
    }
    while (ent != NULL && (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, "..")));
    
    if (ent == NULL)
    {
//        closedir(dir);
        return NULL;
    }
    
    printf("%s; ", ent->d_name);
    sprintf(filename, "%s/%s", folder, ent->d_name);
    
    return LoadImage(filename, reqsize, mean);
}

// LoadImage will read image from disk, convert channels to floats
// subtract network mean for each value in each channel.  Then, convert 
// floats to half precision floats and return pointer to the buffer 
// of half precision floats (Fp16s)
half *ImageFolderFP16::LoadImage(const char *path, int reqsize, float *mean)
{
	int width, height, cp, i;
	unsigned char *img, *imgresized;
	float *imgfp32;
	half *imgfp16;

	img = stbi_load(path, &width, &height, &cp, 3);
	if(!img)
	{
		printf("The picture %s could not be loaded\n", path);
		return 0;
	}
	imgresized = (unsigned char*) malloc(3*reqsize*reqsize);
	if(!imgresized)
	{
		free(img);
		perror("malloc");
		return 0;
	}
	stbir_resize_uint8(img, width, height, 0, imgresized, reqsize, reqsize, 0, 3);
	free(img);
	imgfp32 = (float*) malloc(sizeof(*imgfp32) * reqsize * reqsize * 3);
	if(!imgfp32)
	{
		free(imgresized);
		perror("malloc");
		return 0;
	}
	for(i = 0; i < reqsize * reqsize * 3; i++)
		imgfp32[i] = imgresized[i];
	free(imgresized);
	imgfp16 = (half*) malloc(sizeof(*imgfp16) * reqsize * reqsize * 3);
	if(!imgfp16)
	{
		free(imgfp32);
		perror("malloc");
		return 0;
	}
	for(i = 0; i < reqsize*reqsize; i++)
	{
		float blue, green, red;
        blue = imgfp32[3*i+2];
        green = imgfp32[3*i+1];
        red = imgfp32[3*i+0];

        imgfp32[3*i+0] = blue-mean[0];
        imgfp32[3*i+1] = green-mean[1]; 
        imgfp32[3*i+2] = red-mean[2];

        // uncomment to see what values are getting passed to mvncLoadTensor() before conversion to half float
        //printf("Blue: %f, Grean: %f,  Red: %f \n", imgfp32[3*i+0], imgfp32[3*i+1], imgfp32[3*i+2]);
	}
	
	floattofp16((unsigned char *)imgfp16, imgfp32, 3*reqsize*reqsize);
	free(imgfp32);
	return imgfp16;
}

