#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <memory>
#include <string>
#include <utility>
#include <dirent.h>
#include <float.h>

typedef unsigned short half;
typedef std::pair<std::string, float> Prediction;

#ifdef __cplusplus
extern "C" {
#endif

#define PATH_MAX 4096
#define S_OK     0

#define CHK(_hr) \
{ \
    int hr = _hr; \
    if(hr != S_OK) \
    { \
        errno = (errno == S_OK) ? EPERM : errno; \
        return hr; \
    } \
}

#define CHKB(b) { if(b) { CHK(-1); } }

#ifdef __cplusplus
}
#endif

