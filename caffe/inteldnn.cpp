
#include "inteldnn.hpp"

using namespace caffe;
using namespace MPI;
using std::string;

#define NUM_PREDICTIONS 1

IntelDNN::IntelDNN(const string& _model_file,
                   const string& _trained_file,
                   const string& _mean_file,
                   const string& _label_file,
                   const string& _engine) :
    stopwatch(4)
{
    model_file   = _model_file;
    trained_file = _trained_file;
    mean_file    = _mean_file;
    label_file   = _label_file;
    engine       = _engine;
}

int IntelDNN::initNetwork()
{
    Caffe::set_mode(Caffe::CPU); // <<<<<<<<<<<<<<<<<<<<<<<<<<<<< CPU only!!

    // Load the network
    net_.reset(new Net<float>(model_file, TEST, 0, NULL, NULL, engine));
    net_->CopyTrainedLayersFrom(trained_file);

    CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
    CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

    Blob<float>* input_layer = net_->input_blobs()[0];
    num_channels_ = input_layer->channels();
    CHECK(num_channels_ == 3 || num_channels_ == 1) << "Input layer should have 1 or 3 channels.";
    input_geometry_ = cv::Size(input_layer->width(), input_layer->height());
    
    // Load the binaryproto mean file
    SetMean(mean_file);

    // Load labels
    std::ifstream labels(label_file.c_str());
    CHECK(labels) << "Unable to open labels file " << label_file;
    string line;
    while (std::getline(labels, line))
        labels_.push_back(string(line));

    Blob<float>* output_layer = net_->output_blobs()[0];
    CHECK_EQ(labels_.size(), output_layer->channels()) << "Number of labels is different from the output layer dimension.";
    
    input_layer = net_->input_blobs()[0];
    input_layer->Reshape(1, num_channels_,
                       input_geometry_.height, input_geometry_.width);
    // Forward dimension change to all layers
    net_->Reshape();

    WrapInputLayer();
    
    return 0;
}

int IntelDNN::releaseNetwork()
{
    printf("%.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f\n",
                                                               stopwatch.getMean(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMean(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMean(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMean(3, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(3, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(3, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(3, STOPWATCH_UNIT_MILLISECONDS));
    
    return 0;
}

static bool PairCompare(const std::pair<float, int>& lhs,
                        const std::pair<float, int>& rhs)
{
    return lhs.first > rhs.first;
}

// Return the indices of the top N values of vector v
static std::vector<int> Argmax(const std::vector<float>& v, int N)
{
    std::vector<std::pair<float, int> > pairs;
    for (size_t i = 0; i < v.size(); ++i)
        pairs.push_back(std::make_pair(v[i], i));
    std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

    std::vector<int> result;
    for (int i = 0; i < N; ++i)
        result.push_back(pairs[i].second);
    
    return result;
}

void IntelDNN::preprocess(std::vector<void *> _img)
{
    cv::Mat img = *((cv::Mat *)_img.at(0));
    
    Preprocess(img);
}

// Return the top N predictions
std::vector<Prediction> IntelDNN::classify(std::vector<void *> _img)
{
    // Ignore the _img, as it has already been preprocessed by now

    stopwatch.start(0);
    
    // Perform the inference
    stopwatch.start(1);
    net_->Forward();
    stopwatch.stop(1);
    stopwatch.start(2);
    stopwatch.stop(2);

    // Copy the output layer to a std::vector 
    stopwatch.start(3);
    Blob<float>* output_layer = net_->output_blobs()[0];
    const float* begin = output_layer->cpu_data();
    const float* end = begin + output_layer->channels();
    
    std::vector<float> output = std::vector<float>(begin, end);

    int N = std::min<int>(labels_.size(), NUM_PREDICTIONS);
    std::vector<int> maxN = Argmax(output, N);
    std::vector<Prediction> predictions;
    for (int i = 0; i < N; ++i)
    {
        int idx = maxN[i];
        predictions.push_back(std::make_pair(labels_[idx], output[idx]));
    }
    stopwatch.stop(3);
    stopwatch.stop(0);

    return predictions;
}

// Load the mean file in binaryproto format
void IntelDNN::SetMean(const string& mean_file)
{
    BlobProto blob_proto;
    ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

    // Convert from BlobProto to Blob<float> 
    Blob<float> mean_blob;
    mean_blob.FromProto(blob_proto);
    CHECK_EQ(mean_blob.channels(), num_channels_)
    << "Number of channels of mean file doesn't match input layer.";

    // The format of the mean file is planar 32-bit float BGR or grayscale
    std::vector<cv::Mat> channels;
    float* data = mean_blob.mutable_cpu_data();
    for (int i = 0; i < num_channels_; ++i)
    {
        // Extract an individual channel
        cv::Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
        channels.push_back(channel);
        data += mean_blob.height() * mean_blob.width();
    }

    // Merge the separate channels into a single image
    cv::Mat mean;
    cv::merge(channels, mean);

    // Compute the global mean pixel value and create a mean image
    // filled with this value
    cv::Scalar channel_mean = cv::mean(mean);
    mean_ = cv::Mat(input_geometry_, mean.type(), channel_mean);
}

/* Wrap the input layer of the network in separate cv::Mat objects
* (one per channel). This way we save one memcpy operation and we
* don't need to rely on cudaMemcpy2D. The last preprocessing
* operation will write the separate channels directly to the input
* layer. 
*/
void IntelDNN::WrapInputLayer() 
{
    Blob<float>* input_layer = net_->input_blobs()[0];

    int width = input_layer->width();
    int height = input_layer->height();
    float* input_data = input_layer->mutable_cpu_data();
    for (int i = 0; i < input_layer->channels(); ++i)
    {
        cv::Mat channel(height, width, CV_32FC1, input_data);
        input_channels.push_back(channel);
        input_data += width * height;
    }
}

void IntelDNN::Preprocess(cv::Mat &img)
{
    // Convert the input image to the input image format of the network
    cv::Mat sample;
    if (img.channels() == 3 && num_channels_ == 1)
        cv::cvtColor(img, sample, cv::COLOR_BGR2GRAY);
    else if (img.channels() == 4 && num_channels_ == 1)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2GRAY);
    else if (img.channels() == 4 && num_channels_ == 3)
        cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
    else if (img.channels() == 1 && num_channels_ == 3)
        cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
    else
        sample = img;

    cv::Mat sample_resized;
    if (sample.size() != input_geometry_)
        cv::resize(sample, sample_resized, input_geometry_);
    else
        sample_resized = sample;

    cv::Mat sample_float;
    if (num_channels_ == 3)
        sample_resized.convertTo(sample_float, CV_32FC3);
    else
        sample_resized.convertTo(sample_float, CV_32FC1);

    cv::Mat sample_normalized;
    cv::subtract(sample_float, mean_, sample_normalized);

    // This operation will write the separate BGR planes directly to the
    // input layer of the network because it is wrapped by the cv::Mat
    // objects in input_channels
    cv::split(sample_normalized, input_channels);

    CHECK(reinterpret_cast<float*>(input_channels.at(0).data) == net_->input_blobs()[0]->cpu_data()) << "Input channels are not wrapping the input layer of the network.";
}

