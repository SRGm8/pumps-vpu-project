#ifndef _TARGETDEVICE_CUDNN_H
#define _TARGETDEVICE_CUDNN_H

#include <caffe/caffe.hpp>
#include "stopwatch.hpp"
#include "targetdevice.hpp"

class cuDNN : public TargetDevice
{
    public:
        cuDNN(const std::string& model_file,
                 const std::string& trained_file,
                 const std::string& mean_file,
                 const std::string& label_file);
        virtual ~cuDNN() { }

        virtual int initNetwork();
        virtual int releaseNetwork();
        virtual std::vector<Prediction> classify(std::vector<void *> img);
        void preprocess(std::vector<void *> _img);

    private:
        void SetMean(const std::string& mean_file);
        void WrapInputLayer();
        void Preprocess(cv::Mat &img);
        
        std::shared_ptr< caffe::Net > net_;
        cv::Size input_geometry_;
        int num_channels_;
        cv::Mat mean_;
        std::vector<std::string> labels_;
        std::vector<cv::Mat> input_channels;
        
        std::string model_file;
        std::string trained_file;
        std::string mean_file;
        std::string label_file;
        MPI::Stopwatch stopwatch;
};

#endif

