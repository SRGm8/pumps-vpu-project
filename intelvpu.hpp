#ifndef _TARGETDEVICE_INTELVPU_H
#define _TARGETDEVICE_INTELVPU_H

#include "targetdevice.hpp"

class IntelVPU : public TargetDevice
{
    public:
        IntelVPU(const std::string& graph_file, const std::string& label_file,
                 const int device_id, int reqsize, float *mean);
        virtual ~IntelVPU() { }

        virtual int initNetwork();
        virtual int releaseNetwork();
        virtual std::vector<Prediction> classify(std::vector<void *> img);

    private:
        void *LoadFile(const char *path, unsigned int *length);
        
        std::string graph_file;
        std::string label_file;
        int num_devices;
        int reqsize;
        unsigned int lenBufFp16;
        std::vector<std::string> labels_;
        
        void **deviceHandle;
        void **graphHandle;
        char **devName;
};

#endif

