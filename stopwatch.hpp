#ifndef _STOPWATCH_MPI_H
#define _STOPWATCH_MPI_H

#include <stddef.h>
#include <sys/time.h>
#include <vector>
#include <list>

namespace MPI
{
    enum StopwatchUnit
    {
        STOPWATCH_UNIT_MICROSECONDS = 1,
        STOPWATCH_UNIT_MILLISECONDS = 1000,
        STOPWATCH_UNIT_SECONDS      = 1000000
    };
    
    struct StopwatchMeasurement
    {
        struct timeval start;
        struct timeval stop;
    };
    
    class Stopwatch
    {
        public:
            Stopwatch(size_t size);
            ~Stopwatch();
            
            int start(int id);
            int stop(int id);
            int restart(int id);
            
            double getElapsed(int id, StopwatchUnit unit);
            size_t getElapsedCount(int id);
            double getMean(int id, StopwatchUnit unit);
            double getMeanStd(int id, StopwatchUnit unit);
            double getMin(int id, StopwatchUnit unit);
            double getMax(int id, StopwatchUnit unit);
            
        private:
            typedef std::list<StopwatchMeasurement>        StopwatchMeasurementStore;
            typedef StopwatchMeasurementStore::iterator    StopwatchMeasurementStoreIt;
            typedef std::vector<StopwatchMeasurementStore> StopwatchMeasurementArrayStore;
            
            StopwatchMeasurementArrayStore m_elapsed;
    };
}

#endif

