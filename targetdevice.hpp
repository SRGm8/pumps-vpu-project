#ifndef _TARGETDEVICE_H
#define _TARGETDEVICE_H

#include "common.hpp"

class TargetDevice
{
    public:
        virtual ~TargetDevice() { }
        
        virtual int initNetwork() = 0;
        virtual int releaseNetwork() = 0;
        virtual std::vector<Prediction> classify(std::vector<void *> img) = 0;
};

#endif

