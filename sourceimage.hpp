#ifndef _SOURCEIMAGE_H
#define _SOURCEIMAGE_H

#include "common.hpp"

class SourceImage
{
    public:
        virtual ~SourceImage() { }
        
        virtual void *getNextImage() = 0;
};

#endif

