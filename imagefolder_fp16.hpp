#ifndef _SOURCEIMAGE_IMAGEFOLDER_H
#define _SOURCEIMAGE_IMAGEFOLDER_H

#include "sourceimage.hpp"

class ImageFolderFP16 : public SourceImage
{
    public:
        ImageFolderFP16(const char *folder, int reqsize, float *mean);
        virtual ~ImageFolderFP16() { }

        virtual void *getNextImage();

    private:
        static half *LoadImage(const char *path, int reqsize, float *mean);
        
        DIR *dir;
        char folder[PATH_MAX];
        int reqsize;
        float mean[3];
};

#endif

