
CPP     = g++ -std=c++11 -Wall --pedantic
INCDIR  = -I./
LIBDIR  = -L./
BINDIR  = ./bin
OBJDIR  = $(BINDIR)/obj
LIBS    = -lmvnc
CFLAGS  = $(INCDIR) $(LIBDIR)
MKDIR   = mkdir -p

all: setup main.out

main.out: fp16.o imagefolder_fp16.o intelvpu.o stopwatch.o
	@$(CPP) $(CFLAGS) main.cpp -o $(BINDIR)/main.out $(OBJDIR)/*.o \
			$(LIBS) $(NVLIBS)

stopwatch.o:
	@$(CPP) $(CFLAGS) -c stopwatch.cpp -o $(OBJDIR)/stopwatch.o

intelvpu.o:
	@$(CPP) $(CFLAGS) -c intelvpu.cpp -o $(OBJDIR)/intelvpu.o

imagefolder_fp16.o:
	@$(CPP) $(CFLAGS) -c imagefolder_fp16.cpp -o $(OBJDIR)/imagefolder_fp16.o

fp16.o:
	@$(CPP) $(CFLAGS) -c fp16.c -o $(OBJDIR)/fp16.o
	
setup:
	@$(MKDIR) $(BINDIR) $(OBJDIR)

clean:
	@$(RM) -rf $(BINDIR) $(OBJDIR)
	
rebuild: clean all

