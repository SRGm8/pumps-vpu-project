
#include "fp16.h"
#include <mvnc.h>
#include "stopwatch.hpp"
#include "intelvpu.hpp"

using namespace MPI;
using std::string;

Stopwatch stopwatch(4);

IntelVPU::IntelVPU(const std::string& _graph_file, const std::string& _label_file,
                   const int _num_devices, int _reqsize, float *_mean)
{
    graph_file  = _graph_file;
    label_file  = _label_file;
    num_devices = _num_devices;
    reqsize     = _reqsize;

    // Calculate the length of the buffer that contains the half precision floats.
    // 3 channels * width * height * sizeof a 16bit float 
    lenBufFp16   = 3*reqsize*reqsize*sizeof(half);
    deviceHandle = (void **)malloc(sizeof(void *) * num_devices);
    graphHandle  = (void **)malloc(sizeof(void *) * num_devices);
    devName      = (char **)malloc(sizeof(char *) * num_devices);
}

int IntelVPU::initNetwork()
{
    mvncStatus retCode;
    unsigned int graphFileLen;
    void* graphFileBuf;
    
    // Read in a graph file
    graphFileBuf = LoadFile(graph_file.c_str(), &graphFileLen);
    
    for (int device_id = 0; device_id < num_devices; device_id++)
    {
        devName[device_id] = (char *)malloc(sizeof(char) * PATH_MAX);
        
        retCode = mvncGetDeviceName(device_id, devName[device_id], PATH_MAX);
        if (retCode != MVNC_OK)
        {   // Failed to get device name, maybe none plugged in
            return retCode;
        }
        
        // Try to open the NCS device via the device name
        retCode = mvncOpenDevice(devName[device_id], &deviceHandle[device_id]);
        if (retCode != MVNC_OK)
        {   // Failed to open the device
            return retCode;
        }

        // allocate the graph
        retCode = mvncAllocateGraph(deviceHandle[device_id], &graphHandle[device_id],
                                    graphFileBuf, graphFileLen);
        if (retCode != MVNC_OK)
        {   // Error allocating graph
            return retCode;
        }
    }

    // Load labels
    std::ifstream labels(label_file.c_str());
    std::string line;
    while (std::getline(labels, line))
        labels_.push_back(string(line));
    
    free(graphFileBuf);
    
    return 0;
}

int IntelVPU::releaseNetwork()
{
    mvncStatus retCode;
    
    printf("%.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f; %.6f\n",
                                                               stopwatch.getMean(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(0, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMean(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(1, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMean(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(2, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMean(3, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMeanStd(3, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMin(3, STOPWATCH_UNIT_MILLISECONDS),
                                                               stopwatch.getMax(3, STOPWATCH_UNIT_MILLISECONDS));
    
    for (int device_id = 0; device_id < num_devices; device_id++)
    {
        retCode = mvncDeallocateGraph(graphHandle[device_id]);
        if (retCode != MVNC_OK)
        {
            return retCode;
        } 
        
        retCode = mvncCloseDevice(deviceHandle[device_id]);
        
        if (retCode != MVNC_OK)
        {
            return retCode;
        }
        
        free(devName[device_id]);
    }
    
    free(deviceHandle);
    free(graphHandle);
    free(devName);
    deviceHandle = NULL;
    graphHandle  = NULL;
    devName      = NULL;
    
    return 0;
}

std::vector<Prediction> IntelVPU::classify(std::vector<void *> img)
{
    mvncStatus retCode;
    void **resultData16 = (void **)malloc(sizeof(void *) * num_devices);
    void *userParam;
    unsigned int lenResultData;
    std::vector<Prediction> prediction(num_devices);
    
    stopwatch.start(0);
    
    // start the inference with mvncLoadTensor()
    stopwatch.start(1);
    for (int device_id = 0; device_id < num_devices; device_id++)
    {
        retCode = mvncLoadTensor(graphHandle[device_id], (half *)img.at(device_id),
                                 lenBufFp16, NULL);
    
        if (retCode != MVNC_OK)
        {   // error loading tensor
            return std::vector<Prediction>(0);
        }
    }
    stopwatch.stop(1);
    
    stopwatch.start(2);
    for (int device_id = 0; device_id < num_devices; device_id++)
    {
        retCode = mvncGetResult(graphHandle[device_id], &resultData16[device_id],
                                &lenResultData, &userParam);
        
        if (retCode != MVNC_OK)
        {
            return std::vector<Prediction>(0);
        }
    }
    stopwatch.stop(2);
    
    // Convert half precision floats to full floats
    stopwatch.start(3);
    for (int device_id = 0; device_id < num_devices; device_id++)
    {
        int numResults = lenResultData / sizeof(half);
        float *resultData32 = (float *)malloc(sizeof(float) * numResults);
        fp16tofloat(resultData32, (unsigned char*)resultData16[device_id], numResults);

        prediction[device_id].second = 0.0;
        for (int index = 0; index < numResults; index++)
        {
            // printf("Category %d is: %f\n", index, resultData32[index]);
            if (resultData32[index] > prediction[device_id].second)
            {
                prediction[device_id] = std::make_pair(labels_[index], resultData32[index]);
            }
        }
        
//        free(resultData16[device_id]);
        free(resultData32);
    }
    stopwatch.stop(3);
    stopwatch.stop(0);
    
    free(resultData16);
    
    return prediction;
}

void *IntelVPU::LoadFile(const char *path, unsigned int *length)
{
	FILE *fp;
	char *buf;

	fp = fopen(path, "rb");
	if(fp == NULL)
		return 0;
	fseek(fp, 0, SEEK_END);
	*length = ftell(fp);
	rewind(fp);
	if(!(buf = (char*) malloc(*length)))
	{
		fclose(fp);
		return 0;
	}
	if(fread(buf, 1, *length, fp) != *length)
	{
		fclose(fp);
		free(buf);
		return 0;
	}
	fclose(fp);
	
    // Note: Caller must free the buffer returned!
	return buf;
}

